import React from 'react';

import { Container } from './styles';
import ImgLoading from '../../assets/loading.gif';

const Loading: React.FC = () => (
  <Container>
    <img src={ImgLoading} alt="Loading" />
  </Container>
);

export default Loading;

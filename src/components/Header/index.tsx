import React from 'react';
import { NavLink } from 'react-router-dom';
import { FiPower } from 'react-icons/fi';

import 'react-day-picker/lib/style.css';

import logoImg from '../../assets/logo-amazon.png';
import userImg from '../../assets/user.png';

import { useAuth } from '../../hooks/auth';

import { HeaderComponent, HeaderContent, Profile } from './styles';

const Header: React.FC = () => {
  const { signOut, user } = useAuth();
  const { name, avatar_url } = user;

  return (
    <HeaderComponent>
      <HeaderContent>
        <nav>
          <NavLink to="/inventory" activeClassName="navbar__link--active">
            <strong>Inventory</strong>
          </NavLink>
          <NavLink to="/suppliers" activeClassName="navbar__link--active">
            <strong>Suppliers</strong>
          </NavLink>
        </nav>

        <Profile>
          <img src={avatar_url || userImg} alt={name} />
          <div>
            <span>Welcome</span>
            <NavLink to="/profile">
              <strong>{name}</strong>
            </NavLink>
          </div>
          <button type="button" onClick={signOut}>
            <FiPower />
          </button>
        </Profile>
      </HeaderContent>
    </HeaderComponent>
  );
};

export default Header;

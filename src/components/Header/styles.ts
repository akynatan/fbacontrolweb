import styled from 'styled-components';

export const HeaderComponent = styled.header`
  padding: 20px 20px;
  background: #053740;
`;

export const HeaderContent = styled.div`
  max-width: 90vw;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;

  nav {
    a {
      color: #25bad0;
      text-decoration: none;
      font-size: 20px;
      border: 1px solid #25bad0;
      border-radius: 20px;
      padding: 10px;
    }

    a + a {
      margin-left: 30px;
    }

    .navbar__link--active {
      background: #fff;
    }
  }
`;

export const Profile = styled.div`
  display: flex;
  align-items: center;
  margin-left: 80px;

  img {
    width: 56px;
    height: 56px;
    border-radius: 50%;
  }

  div {
    display: flex;
    flex-direction: column;
    margin-left: 16px;
    line-height: 24px;
  }

  span {
    color: #f4ede8;
  }

  a {
    text-decoration: none;
    color: #25bad0;

    &:hover {
      opacity: 0.8;
    }
  }

  button {
    margin-left: 20px;
    background: transparent;
    border: 0;
    cursor: pointer;

    svg {
      color: #999591;
      width: 20px;
      height: 20px;
    }
  }
`;

export const Content = styled.main`
  max-width: 1120px;
  margin: 64px auto;
  padding: 0 20px;
  display: flex;
`;

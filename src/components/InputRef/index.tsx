import React, { InputHTMLAttributes, useEffect, useRef } from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { useField } from '@unform/core';

import { Container, Error } from './styles';

interface InputProps extends InputHTMLAttributes<TextFieldProps> {
  name: string;
  label: string;
  defaultValue: string;
  multiline?: boolean;
  style?: object;
  rows?: number;
}

const InputRef: React.FC<InputProps> = ({
  name,
  label,
  defaultValue,
  multiline = false,
  style = {},
  rows = 1,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const { fieldName, registerField } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <div>
      <TextField
        defaultValue={defaultValue}
        inputRef={inputRef}
        style={style}
        name={name}
        label={label}
        multiline={multiline}
        rows={rows}
        variant="outlined"
      />
    </div>
  );
};

export default InputRef;

import styled, { css } from 'styled-components';

import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isField: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: #54757a;
  opacity: 1;
  border-radius: 10px;
  border: 2px solid #fff;
  padding: 16px;
  width: 100%;
  display: flex;
  align-items: center;
  color: #fff;

  ${props =>
    props.isErrored &&
    css`
      border-color: #c53030;
    `}

  ${props =>
    props.isFocused &&
    css`
      color: #25bad0;
      border-color: #053740;
    `}

  ${props =>
    props.isField &&
    css`
      color: #25bad0;
    `}

  input {
    background: transparent;
    flex: 1;
    border: 0;
    color: #053740;

    &::placeholder {
      color: #fff;
    }
  }

  svg {
    margin-right: 16px;
  }

  & + div {
    margin-top: 8px;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;
  svg {
    margin: 0;
  }

  span {
    background: #c53030;
    color: #fff;

    &::before {
      border-color: #c53030 transparent;
    }
  }
`;

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import * as Yup from 'yup';
import MaterialTable from 'material-table';
import { format, parseISO } from 'date-fns';
import Select from 'react-select';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import { useParams, NavLink } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import TabPanel from '../../components/TabPanel';
import InputRef from '../../components/InputRef';
import Button from '../../components/Button';
import { useToast } from '../../hooks/toast';

import getValidationErrors from '../../utils/getValidationErrors';

import api from '../../services/api';

import IconAmazon from '../../assets/icon-amazon.png';
import IconSeller from '../../assets/icon-seller.png';
import IconKeepa from '../../assets/icon-keepa.png';

import {
  Container,
  Content,
  ContainerData,
  ContentData,
  ContentDataTitle,
  InformationProduct,
} from './styles';

interface Supplier {
  id: string;
  name: string;
  note: string;
  id_hubspot: string;
  tel: string;
  mail: string;
  domain: string;
  created_at: string;
  updated_at: string;
  value: string;
  label: string;
}

interface ProductSupplier {
  id: string;
  supplier_id: string;
  product_id: string;
  sku_supplier: string;
  note: string;
  created_at: string;
  updated_at: string;
  suppliers: Supplier;
}

interface Product {
  name: string;
  sku: string;
  asin: string;
  upc: string;
  note: string;
  id: string;
  created_at: string;
  created_at_formatted: string;
  updated_at: string;
  updated_at_formatted: string;
}

interface FormData {
  name: string;
  asin: string;
  sku: string;
  upc: string;
  note: string;
}

// interface Props {
//   value: number;
//   index: number;
// }

// const TabPanel: JSX.Element = ({ props }: any) => {
//   const { children, value, index, ...other } = props;

//   return (
//     <div
//       role="tabpanel"
//       hidden={value !== index}
//       id={`simple-tabpanel-${index}`}
//       aria-labelledby={`simple-tab-${index}`}
//       {...other}
//     >
//       {value === index && (
//         <Box p={3}>
//           <Typography>{children}</Typography>
//         </Box>
//       )}
//     </div>
//   );
// };

const ProductsDetail: React.FC = () => {
  const { addToast } = useToast();
  const formRef = useRef<FormHandles>(null);

  const [value, setValue] = useState(0);
  const [loading, setLoading] = useState(true);
  const [product, setProduct] = useState<Product>({} as Product);
  const [suppliers, setSuppliers] = useState<Supplier[]>([]);
  const [productSupplier, setProductSupplier] = useState<ProductSupplier[]>([]);

  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    setLoading(true);
    api
      .get(`/products/detail`, {
        params: {
          product_id: id,
        },
      })
      .then(response => {
        const previousProduct = response.data;
        const productFormatted = {
          ...previousProduct,
          created_at_formatted: format(
            parseISO(previousProduct.created_at),
            'dd/MM/yyyy - hh:mm:ss',
          ),
          updated_at_formatted: format(
            parseISO(previousProduct.updated_at),
            'dd/MM/yyyy - hh:mm:ss',
          ),
        };
        setProduct(productFormatted);
      });

    api
      .get(`/products/suppliers`, {
        params: {
          product_id: id,
        },
      })
      .then(response => {
        setProductSupplier(response.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  useEffect(() => {
    setLoading(true);
    api.get(`/suppliers`).then(response => {
      const suppliers_response = response.data.map((supplier: Supplier) => ({
        ...supplier,
        label: supplier.name,
        value: supplier.id,
      }));

      setSuppliers(suppliers_response);
    });
  }, []);

  const handleChange = (_: any, newValue: number): void => {
    setValue(newValue);
  };

  const suppliersOptions = useMemo(() => {
    const product_supplier_id = productSupplier.map(
      product_supplier => product_supplier.suppliers.id,
    );

    const retorno = suppliers.filter(
      supplier => !product_supplier_id.includes(supplier.id),
    );

    return retorno;
  }, [suppliers, productSupplier]);

  const handleEditSupplier = useCallback(
    async newData => {
      try {
        const { id: product_supplier_id, note, sku_supplier } = newData;
        const { name } = newData.suppliers;
        let supplier_id = '';
        if (typeof name !== 'string')
          supplier_id = newData.suppliers.name.suppliers.id;
        else supplier_id = newData.suppliers.id;

        api
          .put(`/product_supplier`, {
            product_supplier_id,
            sku_supplier,
            note,
            supplier_id,
            product_id: id,
          })
          .then(response => {
            addToast({
              title: 'Supplier eddited!',
            });

            const supplier = suppliers.find(
              supplier_current => supplier_current.id === supplier_id,
            );

            if (!supplier) {
              return;
            }

            const newProductSupplier = response.data;

            setProductSupplier(oldProductSupplier => {
              const retorno = oldProductSupplier.map(product_supplier => {
                if (product_supplier.id !== product_supplier_id) {
                  return product_supplier;
                }
                return { ...newProductSupplier, suppliers: supplier };
              });

              return retorno;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to edit supplier.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to edit supplier.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast, suppliers, id],
  );

  const handleAddSupplier = useCallback(
    async newData => {
      try {
        const { note, sku_supplier } = newData;
        const supplier_id = newData.suppliers.name.suppliers.id;
        api
          .post(`/product_supplier`, {
            sku_supplier,
            product_id: id,
            note,
            supplier_id,
          })
          .then(response => {
            addToast({
              title: 'Supplier added!',
            });

            const supplier = suppliers.find(
              supplier_current => supplier_current.id === supplier_id,
            );

            if (!supplier) {
              return;
            }

            const newProductSupplier = response.data;

            setProductSupplier(oldProductSupplier => [
              ...oldProductSupplier,
              { ...newProductSupplier, suppliers: supplier },
            ]);
          })
          .catch(() => {
            addToast({
              title: 'Failed to add supplier.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to add supplier.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [id, addToast, suppliers],
  );

  const handleSubmit = useCallback(
    async (data: FormData) => {
      console.log(formRef.current);
      console.log(formRef.current?.getData());
      // formRef.current?.setErrors({});
      try {
        // const schema = Yup.object().shape({
        //   name: Yup.string().required('Name required.'),
        // });

        console.log(data);
        const { name, asin, sku, upc, note } = data;

        api
          .put('/products', {
            product_id: id,
            name,
            asin,
            sku,
            upc,
            note,
          })
          .then(response => {
            const previousProduct = response.data;
            const productFormatted = {
              ...previousProduct,
              created_at_formatted: format(
                parseISO(previousProduct.created_at),
                'dd/MM/yyyy hh:mm:ss',
              ),
              updated_at_formatted: format(
                parseISO(previousProduct.updated_at),
                'dd/MM/yyyy - hh:mm:ss',
              ),
            };
            setProduct(productFormatted);

            addToast({
              title: 'Product updated!',
            });
          });

        // await schema.validate(data, {
        //   abortEarly: false,
        // });
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getValidationErrors(err);
          formRef.current?.setErrors(errors);
          return;
        }

        addToast({
          type: 'error',
          title: 'Failed to edit product',
          description: 'An error occurred while editing product information',
        });
      }
    },
    [addToast, id],
  );

  const handleDeleteSupplier = useCallback(
    async product_supplier => {
      const result = confirm('Do you really want to unlink this product?');

      if (!result) return;

      try {
        const product_supplier_id = product_supplier.id;
        api
          .delete(`/product_supplier`, {
            data: { product_supplier_id },
          })
          .then(() => {
            addToast({
              title: 'Supplier unlinked!',
            });

            setProductSupplier(oldProductSupplier => {
              const newProductsSuppliers = oldProductSupplier.filter(
                product_supplier_current =>
                  product_supplier_current.id !== product_supplier_id,
              );

              return newProductsSuppliers;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to unlink supplier.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to unlink supplier.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  const links = useMemo(() => {
    const { asin, sku } = product;
    return {
      amazon: `https://www.amazon.com/dp/${asin}/`,
      seller: `https://sellercentral.amazon.com/skucentral?mSku=${sku}&ref=myi_skuc`,
      keepa: `https://keepa.com/#!product/1-${asin}`,
    };
  }, [product]);

  function a11yProps(index: number): any {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  return (
    <Container>
      <Content>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <h1>{loading ? 'Product' : `${product && product.name}`}</h1>
          <div>
            <a href={links.amazon} target="_blank" rel="noopener noreferrer">
              <img src={IconAmazon} alt="Icon Amazon" width={31} />
            </a>
            <a href={links.seller} target="_blank" rel="noopener noreferrer">
              <img src={IconSeller} alt="Icon Seller Central" width={28} />
            </a>
            <a href={links.keepa} target="_blank" rel="noopener noreferrer">
              <img src={IconKeepa} alt="Icon Keepa" width={32} />
            </a>
          </div>
        </div>
        <ContainerData>
          <ContentData>
            <ContentDataTitle>
              <h3>General information</h3>
              <div>
                <Button
                  type="submit"
                  onClick={() => {
                    formRef.current?.submitForm();
                  }}
                >
                  Save
                </Button>
              </div>
            </ContentDataTitle>
            {loading ? (
              <div>Carregando...</div>
            ) : (
              <InformationProduct>
                <Form ref={formRef} onSubmit={handleSubmit} className="parent">
                  <div className="div1">
                    <InputRef
                      defaultValue={product.name}
                      name="name"
                      label="Name"
                      style={{ width: '100%' }}
                    />
                  </div>
                  <div className="div2">
                    <InputRef
                      defaultValue={product.asin}
                      name="asin"
                      label="ASIN"
                    />
                  </div>
                  <div className="div3">
                    <InputRef
                      defaultValue={product.sku}
                      name="sku"
                      label="SKU"
                    />
                  </div>
                  <div className="div4">
                    <InputRef
                      defaultValue={product.upc}
                      name="upc"
                      label="UPC"
                    />
                  </div>
                  <div className="div5">
                    <InputRef
                      defaultValue={product.note}
                      name="note"
                      label="Note"
                      multiline
                      rows={5}
                      style={{ width: '100%' }}
                    />
                  </div>
                </Form>
              </InformationProduct>
            )}
          </ContentData>

          <div>
            <AppBar position="static">
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="simple tabs example"
                style={{
                  backgroundColor: '#fff',
                  color: '#000',
                }}
              >
                <Tab label="Suppliers" {...a11yProps(0)} />
                <Tab label="Orders History" {...a11yProps(1)} />
                <Tab label="Backorders" {...a11yProps(2)} />
                <Tab label="Shipment History" {...a11yProps(3)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <ContentData>
                <MaterialTable
                  style={{
                    background: '#fff',
                    marginTop: '20px',
                    color: '#000',
                  }}
                  options={{
                    showTitle: false,
                    exportButton: true,
                    actionsColumnIndex: -1,
                    pageSizeOptions: [10, 20, 50, 100],
                    pageSize: 10,
                    headerStyle: {
                      backgroundColor: '#d6d6d6',
                      color: '#000',
                      padding: '8px',
                      textAlign: 'left',
                      cursor: 'pointer',
                    },
                    rowStyle: {
                      padding: '5px',
                    },
                    actionsCellStyle: {
                      color: '#053740',
                    },
                    filterCellStyle: {
                      color: '#053740',
                    },
                    detailPanelColumnStyle: {
                      color: '#053740',
                    },
                    searchFieldStyle: {
                      color: '#053740',
                    },
                    editCellStyle: {
                      color: '#053740',
                    },
                    filterRowStyle: {
                      color: '#053740',
                    },
                  }}
                  actions={[
                    {
                      icon: 'delete',
                      tooltip: 'Delete',
                      onClick: (_, rowData) => handleDeleteSupplier(rowData),
                    },
                  ]}
                  columns={[
                    {
                      title: 'Name Supplier',
                      field: 'suppliers.name',
                      align: 'center',
                      cellStyle: {
                        cursor: 'pointer',
                        padding: '8px',
                        textAlign: 'left',
                      },
                      render: rowData => {
                        const { suppliers: supplierRow } = rowData;
                        const { name, id: id_supplier } = supplierRow;
                        return (
                          <NavLink
                            style={{ textDecoration: 'none', color: '#053740' }}
                            title="Detail Supplier"
                            to={location => {
                              console.log(location);
                              return {
                                ...location,
                                pathname: `suppliers/${id_supplier}`,
                              };
                            }}
                            exact
                          >
                            {name}
                          </NavLink>
                        );
                        console.log(rowData);
                      },
                      editComponent: props => {
                        const { rowData } = props;
                        const { suppliers: supplierOptionValue } = rowData;
                        return (
                          <Select
                            styles={{
                              option: (provided, state) => ({
                                ...provided,
                                backgroundColor: '#fff',
                                borderBottom: '1px dotted #000',
                                color: state.isSelected ? '#ff9000' : '#000',
                                cursor: 'pointer',
                              }),
                              control: provided => ({
                                ...provided,
                                backgroundColor: '#fff',
                                color: '#000',
                              }),
                              singleValue: (provided, state) => {
                                const opacity = state.isDisabled ? 0.5 : 1;
                                const transition = 'opacity 300ms';

                                return {
                                  ...provided,
                                  opacity,
                                  transition,
                                  color: '#000',
                                };
                              },
                            }}
                            options={suppliersOptions}
                            isMulti={false}
                            defaultValue={
                              supplierOptionValue
                                ? {
                                    ...supplierOptionValue,
                                    value: supplierOptionValue.id,
                                    label: supplierOptionValue.name,
                                  }
                                : undefined
                            }
                            onChange={v => props.onChange({ suppliers: v })}
                          />
                        );
                      },
                    },
                    {
                      title: 'SKU Supplier',
                      field: 'sku_supplier',
                      cellStyle: {
                        padding: '8px',
                        textAlign: 'left',
                      },
                    },
                    {
                      title: 'Note',
                      field: 'note',
                      cellStyle: {
                        padding: '8px',
                        textAlign: 'left',
                      },
                    },
                  ]}
                  editable={{
                    onRowAdd: newData => handleAddSupplier(newData),
                    onRowUpdate: newData => handleEditSupplier(newData),
                  }}
                  data={productSupplier}
                />
              </ContentData>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <MaterialTable
                style={{
                  background: '#fff',
                  marginTop: '20px',
                  color: '#000',
                  border: '1px solid #000',
                }}
                options={{
                  showTitle: false,
                }}
                columns={[
                  {
                    title: 'Name Supplier',
                    field: 'suppliers.name',
                    align: 'center',
                    cellStyle: {
                      cursor: 'pointer',
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'SKU Supplier',
                    field: 'sku_supplier',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'Note',
                    field: 'note',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                ]}
                data={[]}
              />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <MaterialTable
                style={{
                  background: '#fff',
                  marginTop: '20px',
                  color: '#000',
                  border: '1px solid #000',
                }}
                options={{
                  showTitle: false,
                }}
                columns={[
                  {
                    title: 'Name Supplier',
                    field: 'suppliers.name',
                    align: 'center',
                    cellStyle: {
                      cursor: 'pointer',
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'SKU Supplier',
                    field: 'sku_supplier',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'Note',
                    field: 'note',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                ]}
                data={[]}
              />
            </TabPanel>
            <TabPanel value={value} index={3}>
              <MaterialTable
                style={{
                  background: '#fff',
                  marginTop: '20px',
                  color: '#000',
                  border: '1px solid #000',
                }}
                options={{
                  showTitle: false,
                }}
                columns={[
                  {
                    title: 'Name Supplier',
                    field: 'suppliers.name',
                    align: 'center',
                    cellStyle: {
                      cursor: 'pointer',
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'SKU Supplier',
                    field: 'sku_supplier',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'Note',
                    field: 'note',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                ]}
                data={[]}
              />
            </TabPanel>
          </div>
        </ContainerData>
      </Content>
    </Container>
  );
};

export default ProductsDetail;

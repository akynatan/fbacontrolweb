import React, {
  ChangeEvent,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import MaterialTable from 'material-table';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/styles';

import api from '../../services/api';

import { Container } from './styles';
import { useToast } from '../../hooks/toast';

import IconAmazon from '../../assets/icon-amazon.png';
import IconSeller from '../../assets/icon-seller.png';
import IconKeepa from '../../assets/icon-keepa.png';

interface Supplier {
  id: string;
  name: string;
  note: string;
  id_hubspot: string;
  tel: string;
  mail: string;
  domain: string;
  created_at: string;
  updated_at: string;
}

interface ProductSupplier {
  id: string;
  supplier_id: string;
  product_id: string;
  sku_supplier: string;
  note: string;
  created_at: string;
  updated_at: string;
  suppliers: Supplier;
}

interface Product {
  name: string;
  sku: string;
  asin: string;
  upc: string;
  note: string;
  id: string;
  product_suppliers: ProductSupplier[];
  name_suppliers: string[];
}

const useStyles = makeStyles({
  colHeader: {
    '&:hover': {
      cursor: 'pointer',
      color: '#fff',
    },
  },
});

const Products: React.FC = () => {
  const { addToast } = useToast();
  const classes = useStyles();
  const inputRef = useRef<HTMLInputElement>(null);

  const [products, setProducts] = useState<Product[]>([]);
  const [suppliers, setSuppliers] = useState<Supplier[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    api.get(`/products`).then(response => {
      setProducts(response.data);
    });
  }, []);

  useEffect(() => {
    setLoading(true);
    api.get(`/suppliers`).then(response => {
      const suppliers_response = response.data.map((supplier: Supplier) => ({
        ...supplier,
        label: supplier.name,
        value: supplier.id,
      }));

      setSuppliers(suppliers_response);
    });
  }, []);

  const productsRow = useMemo(() => {
    const newProducts = products.map(product => {
      const nameSuppliers = product.product_suppliers.map(
        product_supplier => product_supplier.suppliers.name,
      );

      return { ...product, name_suppliers: nameSuppliers.join(', ') };
    });
    return newProducts;
  }, [products]);

  const handleDeleteProduct = useCallback(
    async product => {
      const result = confirm('Do you really want delete this product?');

      if (!result) return;

      try {
        const { id } = product;
        api
          .delete(`/products`, {
            data: { product_id: id },
          })
          .then(() => {
            addToast({
              title: 'Product deleted!',
            });
            setProducts(oldProducts => {
              const newProducts = oldProducts.filter(
                product_current => product_current.id !== id,
              );
              return newProducts;
            });
          })
          .catch(err => {
            if (err.response.status === 400) {
              addToast({
                title: 'Failed to delete product.',
                description: 'Product is associated with an order or supplier.',
                type: 'error',
              });
            } else {
              addToast({
                title: 'Failed to delete product.',
                type: 'error',
              });
            }
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to delete product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  const handleEditProduct = useCallback(
    async newData => {
      try {
        const { id, name, asin, sku, upc, note, product_suppliers } = newData;
        console.log(newData);
        api
          .put(`/products`, {
            product_id: id,
            name,
            asin,
            sku,
            upc,
            note,
          })
          .then(response => {
            addToast({
              title: 'Product eddited!',
            });

            const newProduct = response.data;

            setProducts(oldProducts => {
              const newProducts = oldProducts.map(product =>
                product.id === newProduct.id
                  ? { ...newProduct, product_suppliers }
                  : product,
              );
              return newProducts;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to edit product.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to edit product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  const handleAddProduct = useCallback(
    async newData => {
      try {
        const { name, asin, sku, upc, note } = newData;
        api
          .post(`/products`, {
            name,
            asin,
            sku,
            upc,
            note,
            suppliers: [],
          })
          .then(response => {
            addToast({
              title: 'Product added!',
            });

            const newProduct = response.data;

            setProducts(oldProducts => [
              { ...newProduct, product_suppliers: [] },
              ...oldProducts,
            ]);
          })
          .catch(() => {
            addToast({
              title: 'Failed to add product.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to add product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  const handleUploadChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const data = new FormData();

      const { files } = e.target;
      if (files) {
        data.append('products', files[0]);

        api
          .post('/products/upload', data)
          .then(response => {
            const newProducts = response.data.map((product: Product) => ({
              ...product,
              product_suppliers: [],
            }));

            setProducts(oldProducts => [...newProducts, ...oldProducts]);

            addToast({
              title: 'Successful product upload!',
            });
          })
          .catch(err => {
            console.log(err);
            addToast({
              type: 'error',
              title: 'Failed to upload products!',
            });
          })
          .finally(() => {
            e.target.files = null;
            e.target.value = '';
          });
      }
    },
    [addToast],
  );

  return (
    <Container>
      <MaterialTable
        title="Products"
        style={{ background: '#fff', marginTop: '20px', color: '#000' }}
        options={{
          exportButton: true,
          exportCsv: (columns, data) => {
            const dataExport = data.map(product => {
              const { name, sku, asin, upc, note, name_suppliers } = product;
              return { name, sku, asin, upc, note, name_suppliers };
            });

            const fileType =
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            const fileExtension = '.xlsx';
            const ws = XLSX.utils.json_to_sheet(dataExport);
            const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
            const excelBuffer = XLSX.write(wb, {
              bookType: 'xlsx',
              type: 'array',
            });
            const dataFile = new Blob([excelBuffer], { type: fileType });
            FileSaver.saveAs(dataFile, `Products${fileExtension}`);
          },
          columnsButton: true,
          actionsColumnIndex: -1,
          pageSizeOptions: [10, 20, 50, 100],
          pageSize: 10,
          headerStyle: {
            backgroundColor: '#d6d6d6',
            color: '#000',
            padding: '8px',
            textAlign: 'left',
            cursor: 'pointer',
          },
          rowStyle: {
            padding: '5px',
          },
          actionsCellStyle: {
            color: '#053740',
          },
          filterCellStyle: {
            color: '#053740',
          },
          detailPanelColumnStyle: {
            color: '#053740',
          },
          searchFieldStyle: {
            color: '#053740',
          },
          editCellStyle: {
            color: '#053740',
          },
          filterRowStyle: {
            color: '#053740',
          },
        }}
        actions={[
          {
            icon: 'delete',
            tooltip: 'Delete',
            onClick: (_, rowData) => handleDeleteProduct(rowData),
          },
          {
            icon: 'upload',
            tooltip: 'Upload Products',
            onClick: (_, rowData) => {
              inputRef.current?.click();
            },
            position: 'toolbar',
          },
        ]}
        columns={[
          {
            title: <span className={classes.colHeader}>Name</span>,
            field: 'name',
            align: 'center',
            cellStyle: {
              cursor: 'pointer',
              padding: '8px',
              textAlign: 'left',
            },
            render: rowData => {
              const { name, id } = rowData;
              return (
                <Link
                  style={{ textDecoration: 'none', color: '#053740' }}
                  to={`inventory/${id}`}
                  title="Detail Product"
                >
                  {name}
                </Link>
              );
            },
          },
          {
            title: 'ASIN',
            field: 'asin',
            cellStyle: {
              padding: '8px',
              textAlign: 'left',
            },
          },
          {
            title: 'SKU',
            field: 'sku',
            width: 'auto',
            cellStyle: {
              padding: '8px',
              textAlign: 'left',
            },
          },
          {
            title: 'UPC',
            field: 'upc',
            cellStyle: {
              padding: '8px',
              textAlign: 'left',
            },
          },
          {
            title: 'Note',
            field: 'note',
            cellStyle: {
              padding: '8px',
              textAlign: 'left',
            },
          },
          {
            title: 'Suppliers',
            field: 'name_suppliers',
            cellStyle: {
              padding: '8px',
              textAlign: 'left',
            },
            editable: 'never',
          },
          {
            field: 'links',
            width: 'auto',
            cellStyle: {
              cursor: 'pointer',
              padding: '8px',
              textAlign: 'left',
            },
            editable: 'never',
            render: rowData => {
              const { asin, sku } = rowData;
              const link_amazon = `https://www.amazon.com/dp/${asin}/`;
              const link_seller = `https://sellercentral.amazon.com/skucentral?mSku=${sku}&ref=myi_skuc`;
              const link_keepa = `https://keepa.com/#!product/1-${asin}`;

              return (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <a
                    href={link_amazon}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={IconAmazon} alt="Icon Amazon" width={20} />
                  </a>
                  <a
                    href={link_seller}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img
                      src={IconSeller}
                      alt="Icon Seller Central"
                      width={18}
                    />
                  </a>
                  <a
                    href={link_keepa}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <img src={IconKeepa} alt="Icon Keepa" width={20} />
                  </a>
                </div>
              );
            },
          },
          // editComponent: props => {
          //   const { rowData } = props;
          //   const { product_suppliers } = rowData;

          //   const values = product_suppliers.map(
          //     product_supplier_current => product_supplier_current.suppliers,
          //   );

          //   return (
          //     <Select
          //       styles={{
          //         option: (provided, state) => ({
          //           ...provided,
          //           backgroundColor: '#312E38',
          //           borderBottom: '1px dotted pink',
          //           color: state.isSelected ? '#ff9000' : 'white',
          //           cursor: 'pointer',
          //         }),
          //         control: provided => ({
          //           ...provided,
          //           backgroundColor: '#312E38',
          //           color: '#fff',
          //         }),
          //         singleValue: (provided, state) => {
          //           const opacity = state.isDisabled ? 0.5 : 1;
          //           const transition = 'opacity 300ms';

          //           return {
          //             ...provided,
          //             opacity,
          //             transition,
          //             color: '#fff',
          //           };
          //         },
          //       }}
          //       options={suppliers}
          //       isMulti
          //       defaultValue={values || undefined}
          //       // onChange={v => props.onChange({ suppliers: v })}
          //     />
          //   );
          // },
        ]}
        editable={{
          onRowAdd: newData => handleAddProduct(newData),
          onRowUpdate: newData => handleEditProduct(newData),
        }}
        data={productsRow}
      />
      <input
        id="products"
        ref={inputRef}
        type="file"
        onChange={handleUploadChange}
        style={{ visibility: 'hidden' }}
      />
    </Container>
  );
};

export default Products;

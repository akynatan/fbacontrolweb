import styled from 'styled-components';

export const Container = styled.div`
  margin: 10px auto 44px auto;
  max-width: 90vw;
`;

export const Content = styled.main`
  max-width: 90vw;
  margin: 44px auto;
  display: flex;
`;

import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import * as Yup from 'yup';
import MaterialTable from 'material-table';
import { format, parseISO } from 'date-fns';
import Select from 'react-select';
import { FormHandles } from '@unform/core';
import { Form } from '@unform/web';
import { useParams } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';

import TabPanel from '../../components/TabPanel';
import InputRef from '../../components/InputRef';
import Input from '../../components/Input';
import Button from '../../components/Button';
import { useToast } from '../../hooks/toast';

import api from '../../services/api';

import IconHubspot from '../../assets/icon-hubspot.webp';

import {
  Container,
  Content,
  ContainerData,
  ContentData,
  ContentDataTitle,
  InformationProduct,
  LabelInput,
} from './styles';

interface Supplier {
  id: string;
  name: string;
  note: string;
  id_hubspot: string;
  tel: string;
  mail: string;
  domain: string;
  link_hubspot: string;
  created_at: string;
  created_at_formatted: string;
  updated_at: string;
  updated_at_formatted: string;
}

interface Product {
  name: string;
  sku: string;
  asin: string;
  upc: string;
  note: string;
  id: string;
  created_at: string;
  updated_at: string;
  value: string;
  label: string;
}

interface ProductSupplier {
  id: string;
  supplier_id: string;
  product_id: string;
  sku_supplier: string;
  note: string;
  created_at: string;
  updated_at: string;
  products: Product;
}

interface FormData {
  name: string;
  asin: string;
  sku: string;
  upc: string;
  note: string;
}

const SuppliersDetail: React.FC = () => {
  const { addToast } = useToast();
  const formRef = useRef<FormHandles>(null);

  const [value, setValue] = useState(0);
  const [loading, setLoading] = useState(true);
  const [supplier, setSupplier] = useState<Supplier>({} as Supplier);
  const [products, setProducts] = useState<Product[]>([]);
  const [supplierProduct, setSupplierProduct] = useState<ProductSupplier[]>([]);

  const { id } = useParams<{ id: string }>();

  useEffect(() => {
    setLoading(true);
    api
      .get(`/suppliers/detail`, {
        params: {
          supplier_id: id,
        },
      })
      .then(response => {
        const previousSupplier = response.data;
        const supplierFormatted = {
          ...previousSupplier,
          created_at_formatted: format(
            parseISO(previousSupplier.created_at),
            'dd/MM/yyyy - hh:mm:ss',
          ),
          updated_at_formatted: format(
            parseISO(previousSupplier.updated_at),
            'dd/MM/yyyy - hh:mm:ss',
          ),
        };
        setSupplier(supplierFormatted);
      });

    api
      .get(`/suppliers/products`, {
        params: {
          supplier_id: id,
        },
      })
      .then(response => {
        setSupplierProduct(response.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  useEffect(() => {
    setLoading(true);
    api.get(`/products`).then(response => {
      const products_response = response.data.map((product: Product) => ({
        ...product,
        label: product.name,
        value: product.id,
      }));

      setProducts(products_response);
    });
  }, []);

  const productsOptions = useMemo(() => {
    const product_supplier_id = supplierProduct.map(
      product_supplier => product_supplier.products.id,
    );

    const retorno = products.filter(
      product => !product_supplier_id.includes(product.id),
    );
    console.log(retorno);
    return retorno;
  }, [products, supplierProduct]);

  const handleEditProduct = useCallback(
    async newData => {
      try {
        const { id: product_supplier_id, note, sku_supplier } = newData;
        const { name } = newData.products;
        let product_id = '';
        if (typeof name !== 'string')
          product_id = newData.products.name.products.id;
        else product_id = newData.products.id;

        api
          .put(`/product_supplier`, {
            product_supplier_id,
            sku_supplier,
            note,
            supplier_id: id,
            product_id,
          })
          .then(response => {
            addToast({
              title: 'Product eddited!',
            });

            const product = products.find(
              product_current => product_current.id === product_id,
            );

            if (!product) {
              return;
            }

            const newProductSupplier = response.data;

            setSupplierProduct(oldProductSupplier => {
              const retorno = oldProductSupplier.map(product_supplier => {
                if (product_supplier.id !== product_supplier_id) {
                  return product_supplier;
                }
                return { ...newProductSupplier, products: product };
              });

              return retorno;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to edit product.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to edit product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast, products, id],
  );

  const handleAddProduct = useCallback(
    async newData => {
      try {
        const { note, sku_supplier } = newData;
        const product_id = newData.products.name.products.id;
        api
          .post(`/product_supplier`, {
            sku_supplier,
            product_id,
            note,
            supplier_id: id,
          })
          .then(response => {
            addToast({
              title: 'Product added!',
            });

            const product = products.find(
              supplier_current => supplier_current.id === product_id,
            );

            if (!product) {
              return;
            }

            const newProductSupplier = response.data;

            setSupplierProduct(oldProductSupplier => [
              { ...newProductSupplier, products: product },
              ...oldProductSupplier,
            ]);
          })
          .catch(() => {
            addToast({
              title: 'Failed to add product.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to add product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [id, addToast, products],
  );

  const handleDeleteProduct = useCallback(
    async product_supplier => {
      const result = confirm('Do you really want to unlink this product?');

      if (!result) return;

      try {
        const product_supplier_id = product_supplier.id;
        api
          .delete(`/product_supplier`, {
            data: { product_supplier_id },
          })
          .then(() => {
            addToast({
              title: 'Product unlinked!',
            });

            setSupplierProduct(oldProductSupplier => {
              const newProductsSuppliers = oldProductSupplier.filter(
                product_supplier_current =>
                  product_supplier_current.id !== product_supplier_id,
              );

              return newProductsSuppliers;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to unlink product.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to unlink product.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  const handleSubmit = useCallback(
    async (data: FormData) => {
      console.log(data);
      formRef.current?.setErrors({});
      // try {
      //   const schema = Yup.object().shape({
      //     name: Yup.string().required('Name required.'),
      //   });

      //   const { name, asin, sku, upc, note } = data;

      //   api
      //     .put('/products', {
      //       product_id: id,
      //       name,
      //       asin,
      //       sku,
      //       upc,
      //       note,
      //     })
      //     .then(response => {
      //       const previousProduct = response.data;
      //       const productFormatted = {
      //         ...previousProduct,
      //         created_at_formatted: format(
      //           parseISO(previousProduct.created_at),
      //           'dd/MM/yyyy hh:mm:ss',
      //         ),
      //         updated_at_formatted: format(
      //           parseISO(previousProduct.updated_at),
      //           'dd/MM/yyyy - hh:mm:ss',
      //         ),
      //       };
      //       setProduct(productFormatted);

      //       addToast({
      //         title: 'Product updated!',
      //       });
      //     });

      //   await schema.validate(data, {
      //     abortEarly: false,
      //   });
      // } catch (err) {
      //   if (err instanceof Yup.ValidationError) {
      //     const errors = getValidationErrors(err);
      //     formRef.current?.setErrors(errors);
      //     return;
      //   }

      //   addToast({
      //     type: 'error',
      //     title: 'Failed to edit product',
      //     description: 'An error occurred while editing product information',
      //   });
      // }
    },
    [addToast, id],
  );

  function a11yProps(index: number): any {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  const handleChange = (_: any, newValue: number): void => {
    setValue(newValue);
  };

  return (
    <Container>
      <Content>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <h1>{loading ? 'Supplier' : `${supplier && supplier.name}`}</h1>
          <div>
            <a
              href={`https://app.hubspot.com/contacts/5486671/company/${supplier.id_hubspot}/`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={IconHubspot} alt="Icon Hubspot" width={30} />
            </a>
          </div>
        </div>
        <ContainerData>
          <ContentData>
            <ContentDataTitle>
              <h3>General information</h3>
              <div>
                <Button
                  type="submit"
                  onClick={() => {
                    formRef.current?.submitForm();
                  }}
                >
                  Save
                </Button>
              </div>
            </ContentDataTitle>
            {loading ? (
              <div>Carregando...</div>
            ) : (
              <InformationProduct>
                <Form ref={formRef} onSubmit={handleSubmit} className="parent">
                  <LabelInput className="div1">
                    <div>
                      <span>Tel: </span>
                      <p>{supplier.tel ? supplier.tel : '-'}</p>
                    </div>
                    <div>
                      <span>Domain: </span>
                      <a
                        href={`https://${supplier.domain}`}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {supplier.domain}
                      </a>
                    </div>
                  </LabelInput>
                  <div className="div2">
                    <InputRef
                      defaultValue={supplier.note}
                      name="note"
                      label="Note"
                      multiline
                      rows={5}
                      style={{ width: '100%' }}
                    />
                  </div>
                </Form>
              </InformationProduct>
            )}
          </ContentData>
          <div>
            <AppBar position="static">
              <Tabs
                value={value}
                onChange={handleChange}
                aria-label="simple tabs example"
                style={{
                  backgroundColor: '#fff',
                  color: '#000',
                }}
              >
                <Tab label="Products" {...a11yProps(0)} />
                <Tab label="Orders History" {...a11yProps(1)} />
                <Tab label="Backorders" {...a11yProps(2)} />
                <Tab label="Shipment History" {...a11yProps(3)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <ContentData>
                <MaterialTable
                  style={{
                    background: '#fff',
                    marginTop: '20px',
                    color: '#000',
                  }}
                  options={{
                    showTitle: false,
                    exportButton: true,
                    actionsColumnIndex: -1,
                    pageSizeOptions: [10, 20, 50, 100],
                    pageSize: 10,
                    headerStyle: {
                      backgroundColor: '#d6d6d6',
                      color: '#000',
                      padding: '8px',
                      textAlign: 'left',
                      cursor: 'pointer',
                    },
                    rowStyle: {
                      padding: '5px',
                    },
                    actionsCellStyle: {
                      color: '#053740',
                    },
                    filterCellStyle: {
                      color: '#053740',
                    },
                    detailPanelColumnStyle: {
                      color: '#053740',
                    },
                    searchFieldStyle: {
                      color: '#053740',
                    },
                    editCellStyle: {
                      color: '#053740',
                    },
                    filterRowStyle: {
                      color: '#053740',
                    },
                  }}
                  actions={[
                    {
                      icon: 'delete',
                      tooltip: 'Delete',
                      onClick: (_, rowData) => handleDeleteProduct(rowData),
                    },
                  ]}
                  columns={[
                    {
                      title: 'Name Product',
                      field: 'products.name',
                      align: 'center',
                      cellStyle: {
                        cursor: 'pointer',
                        padding: '8px',
                        textAlign: 'left',
                      },
                      editComponent: props => {
                        const { rowData } = props;
                        const { products: supplierOptionValue } = rowData;
                        return (
                          <Select
                            styles={{
                              option: (provided, state) => ({
                                ...provided,
                                backgroundColor: '#d6d6d6',
                                borderBottom: '1px dotted pink',
                                color: state.isSelected ? '#ff9000' : 'white',
                                cursor: 'pointer',
                              }),
                              control: provided => ({
                                ...provided,
                                backgroundColor: '#d6d6d6',
                                color: '#000',
                              }),
                              singleValue: (provided, state) => {
                                const opacity = state.isDisabled ? 0.5 : 1;
                                const transition = 'opacity 300ms';

                                return {
                                  ...provided,
                                  opacity,
                                  transition,
                                  color: '#000',
                                };
                              },
                            }}
                            options={productsOptions}
                            isMulti={false}
                            defaultValue={
                              supplierOptionValue
                                ? {
                                    ...supplierOptionValue,
                                    value: supplierOptionValue.id,
                                    label: supplierOptionValue.name,
                                  }
                                : undefined
                            }
                            onChange={v => props.onChange({ products: v })}
                          />
                        );
                      },
                    },
                    {
                      title: 'SKU Supplier',
                      field: 'sku_supplier',
                      cellStyle: {
                        padding: '8px',
                        textAlign: 'left',
                      },
                    },
                    {
                      title: 'Note',
                      field: 'note',
                      cellStyle: {
                        padding: '8px',
                        textAlign: 'left',
                      },
                    },
                  ]}
                  editable={{
                    onRowAdd: newData => handleAddProduct(newData),
                    onRowUpdate: newData => handleEditProduct(newData),
                  }}
                  data={supplierProduct}
                />
              </ContentData>
            </TabPanel>

            <TabPanel value={value} index={1}>
              <MaterialTable
                style={{
                  background: '#fff',
                  marginTop: '20px',
                  color: '#000',
                  border: '1px solid #000',
                }}
                options={{
                  showTitle: false,
                }}
                columns={[
                  {
                    title: 'Name Supplier',
                    field: 'suppliers.name',
                    align: 'center',
                    cellStyle: {
                      cursor: 'pointer',
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'SKU Supplier',
                    field: 'sku_supplier',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'Note',
                    field: 'note',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                ]}
                data={[]}
              />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <MaterialTable
                style={{
                  background: '#fff',
                  marginTop: '20px',
                  color: '#000',
                  border: '1px solid #000',
                }}
                options={{
                  showTitle: false,
                }}
                columns={[
                  {
                    title: 'Name Supplier',
                    field: 'suppliers.name',
                    align: 'center',
                    cellStyle: {
                      cursor: 'pointer',
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'SKU Supplier',
                    field: 'sku_supplier',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                  {
                    title: 'Note',
                    field: 'note',
                    cellStyle: {
                      padding: '8px',
                      textAlign: 'left',
                    },
                  },
                ]}
                data={[]}
              />
            </TabPanel>
            <TabPanel value={value} index={3} />
          </div>
        </ContainerData>
      </Content>
    </Container>
  );
};

export default SuppliersDetail;

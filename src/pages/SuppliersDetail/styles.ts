import styled from 'styled-components';

export const Container = styled.div``;

export const Content = styled.main`
  max-width: 90vw;
  margin: 10px auto 44px auto;
  display: flex;
  flex-direction: column;
`;

export const ContainerData = styled.div`
  width: 100%;
`;

export const ContentData = styled.div``;

export const ContentDataTitle = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid #000;
  align-items: flex-end;

  div {
    display: flex;
    justify-content: center;
    align-items: center;

    svg {
      width: 20px;
      height: 20px;
      cursor: pointer;
      margin-left: 10px;
    }

    button {
      height: 32px;
      margin-top: 0;
      margin-bottom: 4px;
    }
  }
`;

export const InformationProduct = styled.div`
  display: flex;
  flex-direction: column;
  line-height: 20px;
  margin: 20px 0;

  span + span {
    margin-top: 4px;
  }

  .parent {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-template-rows: 1fr;
    grid-column-gap: 0px;
    grid-row-gap: 0px;
  }

  .div1 {
    grid-area: 1 / 1 / 2 / 2;
    min-width: 260px;
  }
  .div2 {
    grid-area: 1 / 2 / 2 / 5;
  }
`;

export const LabelInput = styled.div`
  display: flex;
  flex-direction: column;

  div {
    display: flex;

    p,
    a {
      margin-left: 5px;
    }
  }
`;

import React, { useCallback, useEffect, useState } from 'react';
import MaterialTable from 'material-table';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Link } from 'react-router-dom';

import api from '../../services/api';

import { useToast } from '../../hooks/toast';
import Loading from '../../components/Loading';
import IconHubspot from '../../assets/icon-hubspot.webp';

import { Container } from './styles';

interface Supplier {
  id: string;
  name: string;
  note: string;
  id_hubspot: string;
  tel: string;
  mail: string;
  domain: string;
  created_at: string;
  updated_at: string;
}

const Suppliers: React.FC = () => {
  const { addToast } = useToast();

  const [suppliers, setSuppliers] = useState<Supplier[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    api.get(`/suppliers`).then(response => {
      const suppliers_response = response.data.map((supplier: Supplier) => ({
        ...supplier,
        label: supplier.name,
        value: supplier.id,
      }));

      setSuppliers(suppliers_response);
      setLoading(false);
    });
  }, []);

  const handleEditSupplier = useCallback(
    async newData => {
      try {
        const { id, name, tel, mail, domain, note } = newData;
        console.log(newData);
        api
          .put(`/suppliers`, {
            id,
            name,
            tel,
            mail,
            domain,
            note,
          })
          .then(response => {
            addToast({
              title: 'Supplier eddited!',
            });

            const newSupplier = response.data;

            setSuppliers(oldSupplier => {
              const newSuppliers = oldSupplier.map(supplier =>
                supplier.id === newSupplier.id ? newSupplier : supplier,
              );
              return newSuppliers;
            });
          })
          .catch(() => {
            addToast({
              title: 'Failed to edit supplier.',
              type: 'error',
            });
          })
          .finally(() => {
            setLoading(false);
          });
      } catch (err) {
        addToast({
          title: 'Failed to edit supplier.',
          type: 'error',
        });
        console.log(err);
      }
    },
    [addToast],
  );

  return (
    <>
      {loading && <Loading />}
      <Container>
        <MaterialTable
          title="Suppliers"
          style={{ background: '#fff', marginTop: '20px', color: '#000' }}
          options={{
            exportButton: true,
            exportCsv: (_, data) => {
              const dataExport = data.map(product => {
                const { name, tel, mail, domain, note } = product;
                return { name, tel, mail, domain, note };
              });

              const fileType =
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
              const fileExtension = '.xlsx';
              const ws = XLSX.utils.json_to_sheet(dataExport);
              const wb = { Sheets: { data: ws }, SheetNames: ['data'] };
              const excelBuffer = XLSX.write(wb, {
                bookType: 'xlsx',
                type: 'array',
              });
              const dataFile = new Blob([excelBuffer], { type: fileType });
              FileSaver.saveAs(dataFile, `Suppliers${fileExtension}`);
            },
            columnsButton: true,
            actionsColumnIndex: -1,
            pageSizeOptions: [10, 20, 50, 100],
            pageSize: 10,
            headerStyle: {
              backgroundColor: '#d6d6d6',
              color: '#000',
              padding: '8px',
              textAlign: 'left',
              cursor: 'pointer',
            },
            rowStyle: {
              padding: '5px',
            },
            actionsCellStyle: {
              color: '#053740',
            },
            filterCellStyle: {
              color: '#053740',
            },
            detailPanelColumnStyle: {
              color: '#053740',
            },
            searchFieldStyle: {
              color: '#053740',
            },
            editCellStyle: {
              color: '#053740',
            },
            filterRowStyle: {
              color: '#053740',
            },
          }}
          columns={[
            {
              title: 'Name',
              field: 'name',
              align: 'center',
              cellStyle: {
                cursor: 'pointer',
                padding: '8px',
                textAlign: 'left',
                width: 'auto',
              },
              editable: 'never',
              render: rowData => {
                const { name, id } = rowData;
                return (
                  <Link
                    style={{ textDecoration: 'none', color: '#053740' }}
                    to={`suppliers/${id}`}
                    title="Detail Product"
                  >
                    {name}
                  </Link>
                );
              },
            },
            {
              title: 'Domain',
              field: 'domain',
              emptyValue: '-',
              width: 'auto',
              cellStyle: {
                padding: '8px',
                textAlign: 'left',
              },
              editable: 'never',
              render: rowData => (
                <a
                  style={{ textDecoration: 'none', color: '#053740' }}
                  href={`https://${rowData.domain}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {rowData.domain}
                </a>
              ),
            },
            {
              title: 'Note',
              field: 'note',
              emptyValue: '-',
              cellStyle: {
                padding: '8px',
                textAlign: 'left',
                maxWidth: '400px',
              },
            },
            {
              field: 'links',
              width: 'auto',
              cellStyle: {
                cursor: 'pointer',
                padding: '8px',
                textAlign: 'left',
              },
              editable: 'never',
              render: rowData => {
                const { id_hubspot } = rowData;
                const link_hubspot = `https://app.hubspot.com/contacts/5486671/company/${id_hubspot}/`;

                return (
                  <div
                    style={{
                      display: 'flex',
                      alignItems: 'center',
                      maxWidth: '25px',
                    }}
                  >
                    <a
                      href={link_hubspot}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <img src={IconHubspot} alt="Icon Hubspot" width={20} />
                    </a>
                  </div>
                );
              },
            },
          ]}
          editable={{
            onRowUpdate: newData => handleEditSupplier(newData),
          }}
          data={suppliers}
        />
      </Container>
    </>
  );
};

export default Suppliers;

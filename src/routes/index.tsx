import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';

import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import ForgotPassword from '../pages/ForgotPassword';
import ResetPassword from '../pages/ResetPassword';

import Profile from '../pages/Profile';
import Dashboard from '../pages/Dashboard';
import Products from '../pages/Products';
import ProductsDetail from '../pages/ProductsDetail';
import Suppliers from '../pages/Suppliers';
import SuppliersDetail from '../pages/SuppliersDetail';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={SignIn} />
    <Route path="/signup" component={SignUp} />
    <Route path="/forgot-password" component={ForgotPassword} />
    <Route path="/reset-password" component={ResetPassword} />

    <Route path="/dashboard" component={Dashboard} isPrivate />
    <Route path="/inventory/:id" component={ProductsDetail} isPrivate />
    <Route path="/inventory" component={Products} isPrivate />

    <Route path="/suppliers/:id" component={SuppliersDetail} isPrivate />
    <Route path="/suppliers" component={Suppliers} isPrivate />

    <Route path="/profile" component={Profile} isPrivate />
  </Switch>
);

export default Routes;
